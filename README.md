# Excuses project

Excuses project is back-end side part of "Excuses de Dev" generator.

## Install

Python3 and sqlite3 are needed to run the project.

> If you want to use virtual environnements, follow theses steps
>
> Install python-venv :
>
>       apt-get install python3-pip
>       pip3 install -U pip virtualenv
>       python3 -m venv venv
>
> Source venv :
>
>       source venv/bin/activate

Installation of dependencies :

> Install requirements :
>
>       pip3 install -r requirements.txt
>
> Configuration of the model and implementation of the sqlite database :
>
>       cd excuses
>       python3 manage.py makemigrations
>       python3 manage.py migrate
>
> Populate the db
>
>       python3 manage.py populatedb
>
> Launching the development server listening on port 8000 (not to be used for production) :
>
>       python3 manage.py runserver

## Updates

If python libraries are modified, is necessary to update the requirements file.

> Update requirements :
>
>       pip freeze > requirements.txt

## Tests

> To run tests :
>
>       python3 manage.py test

## Framework Documentation

[Djangoproject](https://docs.djangoproject.com/en/4.0/)
