from django.core.exceptions import FieldError


def check_fields(fields):
    for field in fields.items():
        if not field[1]:
            raise FieldError
