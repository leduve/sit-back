from django.db.models import Max

from app.models.Excuse import Excuse

from app.functions.tools import check_fields


def find_all():
    return Excuse.objects.all()


def retrieve_max_http_code():
    return Excuse.objects.aggregate(Max('http_code'))


def create(tag, message, http_code=None):
    params = locals()
    del params['http_code']
    check_fields(params)
    if not http_code:
        http_code = retrieve_max_http_code()['http_code__max'] + 1
    return Excuse.objects.create(http_code=http_code, tag=tag, message=message)


def exist(http_code):
    try:
        excuse = Excuse.objects.get(http_code=http_code)
        return True
    except Excuse.DoesNotExist:
        return False
