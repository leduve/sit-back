from django.core.management.base import BaseCommand, CommandError

from app.functions import excuse as excuses_functions


class Command(BaseCommand):
    help = 'Populate db with excuses'

    def handle(self, *args, **options):

        if excuses_functions.exist(701):
            self.stderr.write('Already populated.')
            return

        tags = ['Inexcusable', 'Novelty', 'Fucking', 'Substance']
        messages = ['Meh', 'Explosion', 'PHP', 'Convenience Store', 'Haskell']

        excuses_functions.create(
            http_code=701, tag=tags[0], message="Goto Fail")

        for tag in tags:
            for message in messages:
                excuses_functions.create(
                    http_code=None, tag=tag, message=message)

        self.stdout.write(self.style.SUCCESS('Success !'))
