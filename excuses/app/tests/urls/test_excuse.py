from django.test import Client, TestCase
from app.models.Excuse import Excuse
from django.db.models import Max
import json


class ExcuseTest(TestCase):
    def setUp(self):
        Excuse.objects.create(http_code=200, tag="my_tag",
                              message="my_message")
        self.client = Client()

    def test_get(self):
        response = self.client.get('/excuse/all/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_add(self):
        response = self.client.post(
            '/excuse/add/', {"tag": "my_tag2", "excuse": "my_message2"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Excuse.objects.count(), 2)
        excuse = Excuse.objects.get(id=2)
        self.assertEqual(excuse.tag, "my_tag2")
        self.assertEqual(excuse.message, "my_message2")

    def test_add_missing_param(self):
        response = self.client.post(
            '/excuse/add/', {"message": "my_message2"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {
                         'error': 'some parameters are missing'})

    def test_add_blank_param(self):
        response = self.client.post(
            '/excuse/add/', {"tag": "", "excuse": "my_message2"})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {
                         'error': 'parameters should not be blank'})

    def test_add_without_http_code(self):
        max = Excuse.objects.aggregate(Max('http_code'))
        response = self.client.post(
            '/excuse/add/', {"tag": "my_tag", "excuse": "my_message2"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(json.loads(response.content)[
                             0]['fields']['http_code']), int(max['http_code__max'] + 1))
