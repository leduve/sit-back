from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest

from django.core import serializers
from django.core.exceptions import FieldError

from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt

from app.functions import excuse as excuses_functions


@csrf_exempt
@require_http_methods(["GET"])
def get(request):
    excuses = excuses_functions.find_all()
    return HttpResponse(serializers.serialize('json', excuses))


@csrf_exempt
@require_http_methods(["POST"])
def add(request):
    try:
        excuse = excuses_functions.create(http_code=None, tag=request.POST.__getitem__(
            'tag'), message=request.POST.__getitem__('excuse'))
        return HttpResponse(serializers.serialize('json', [excuse]))
    except FieldError:
        return HttpResponseBadRequest(JsonResponse({'error': 'parameters should not be blank'}))
    except KeyError:
        return HttpResponseBadRequest(JsonResponse({'error': 'some parameters are missing'}))
