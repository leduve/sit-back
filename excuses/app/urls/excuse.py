from django.urls import path
from app.views import excuse

urlpatterns = [
    path('all/', excuse.get, name='excuse'),
    path('add/', excuse.add, name='excuse')
]
