from django.db import models


class Excuse(models.Model):
    http_code = models.IntegerField()
    tag = models.CharField(max_length=200)
    message = models.CharField(max_length=200)
